'use strict';

var http = require('http');

exports.handler = function (event, context) {
    try {
        console.log("event.session.application.applicationId=" + event.session.application.applicationId);

        // This is development application - replace it with actual application ID upon registration
        if (event.session.application.applicationId !== "amzn1.ask.skill.71c22360-6de6-415f-a0f6-57b5f48f4299") {
            context.fail("Invalid Application ID");
        }

        if (event.session.new) {
            onSessionStarted({requestId: event.request.requestId}, event.session);
        }

        if (event.request.type === "LaunchRequest") {
            onLaunch(event.request,
                event.session,
                function callback(sessionAttributes, speechletResponse) {
                    context.succeed(buildResponse(sessionAttributes, speechletResponse));
                });
        } else if (event.request.type === "IntentRequest") {
            onIntent(event.request,
                event.session,
                function callback(sessionAttributes, speechletResponse) {
                    context.succeed(buildResponse(sessionAttributes, speechletResponse));
                });
        } else if (event.request.type === "SessionEndedRequest") {
            onSessionEnded(event.request, event.session);
            context.succeed();
        }
    } catch (e) {
        context.fail("Exception: " + e);
    }
};

/**
 * Called when the session starts.
 */
function onSessionStarted(sessionStartedRequest, session) {
    console.log("onSessionStarted requestId=" + sessionStartedRequest.requestId
        + ", sessionId=" + session.sessionId);

    // add any session init logic here
}

/**
 * Called when the user invokes the skill without specifying what they want.
 */
function onLaunch(launchRequest, session, callback) {
    console.log("onLaunch requestId=" + launchRequest.requestId
        + ", sessionId=" + session.sessionId);

    getWelcomeResponse(callback);
}

/**
 * Called when the user specifies an intent for this skill.
 */
function onIntent(intentRequest, session, callback) {
    console.log("onIntent requestId=" + intentRequest.requestId
        + ", sessionId=" + session.sessionId);

    var intent = intentRequest.intent,
        intentName = intentRequest.intent.name;

    // handle yes/no intent after the user has been prompted
    if (session.attributes && session.attributes.userPromptedToContinue) {
        delete session.attributes.userPromptedToContinue;
        if ("AMAZON.NoIntent" === intentName) {
            handleFinishSessionRequest(intent, session, callback);
        } else if ("AMAZON.YesIntent" === intentName) {
            handleRepeatRequest(intent, session, callback);
        }
    }

    // dispatch custom intents to handlers here
    if ("TirePressureIntent" === intentName) {
        handleTirePressureRequest(intent, session, callback);
    } else if ("TireTemperatureIntent" === intentName) {
        handleTireTemperatureRequest(intent, session, callback);
    } else if ("AMAZON.YesIntent" === intentName) {
        handleUnsupportedRequest(intent, session, callback);
    } else if ("AMAZON.NoIntent" === intentName) {
        handleUnsupportedRequest(intent, session, callback);
    } else if ("AMAZON.RepeatIntent" === intentName) {
        handleRepeatRequest(intent, session, callback);
    } else if ("AMAZON.HelpIntent" === intentName) {
        handleGetHelpRequest(intent, session, callback);
    } else if ("AMAZON.StopIntent" === intentName) {
        handleFinishSessionRequest(intent, session, callback);
    } else if ("AMAZON.CancelIntent" === intentName) {
        handleFinishSessionRequest(intent, session, callback);
    } else {
        throw "Invalid intent";
    }
}

/**
 * Called when the user ends the session.
 * Is not called when the skill returns shouldEndSession=true.
 */
function onSessionEnded(sessionEndedRequest, session) {
    console.log("onSessionEnded requestId=" + sessionEndedRequest.requestId
        + ", sessionId=" + session.sessionId);

    // Add any cleanup logic here
}

// ------- Skill specific business logic -------

var CARD_TITLE = "FOBO";

function getWelcomeResponse(callback) {
    var sessionAttributes = {},
        speechOutput = "Welcome to FOBO. ",
        shouldEndSession = false,
        repromptText = "How may I help you today? ";

    speechOutput += repromptText;
    sessionAttributes = {
        "speechOutput": repromptText,
        "repromptText": repromptText,
    };
    callback(sessionAttributes,
        buildSpeechletResponse(CARD_TITLE, speechOutput, repromptText, shouldEndSession));
}

function handleTirePressureRequest(intent, session, callback) {
    // Ensure that session.attributes has been initialized
    if (!session.attributes) {
        session.attributes = {};
    }

    // Set a flag to track that we're in the Help state.
    session.attributes.userPromptedToContinue = true;

    var url = "http://www.fobocloud.com/Fobo7/index.php/api/report/alexa";
    http.get(url, function(res) { 
        res.setEncoding('utf8'); 
        res.on('data', function(chunk) {
            var resultObj;
            var shouldEndSession = false;
            var speechOutput = ""; 
            var repromptText = "Please ask me for the state of one of your tires. "; 
            var position = "";
            var verticalPositionSlot = intent.slots.VerticalPosition.value;
            switch (verticalPositionSlot) {
                case "front":
                case "lead":
                case "leading":
                    position += "F";
                    break;
                case "back":
                case "aft":
                case "rear":
                    position += "R";
                    break;
            }
            var horizontalPositionSlot = intent.slots.HorizontalPosition.value;
            switch (horizontalPositionSlot) {
                case "left":
                case "left hand":
                case "left side":
                case "left hand side":
                    position += "L";
                    break;
                case "right":
                case "right hand":
                case "right side":
                case "right hand side":
                    position += "R";
                    break;
            }
            if (chunk) {
                resultObj = JSON.parse("" + chunk);
            } else {
                console.log("Unable to resolve web request to Fobo.");
            }
            if (resultObj) {
                shouldEndSession = true;
                var pressure = resultObj[position]["pressure"];
                speechOutput = "Your " + verticalPositionSlot + " " + horizontalPositionSlot
                    + " tire has a pressure of " + pressure + ". ";
            }
            if (!speechOutput) { 
                speechOutput = "I was unable to process your request. "; 
            } 
            callback(session.attributes,
                buildSpeechletResponseWithoutCard(speechOutput, repromptText, shouldEndSession));
        });
    }).on('error', function (e) {
        var shouldEndSession = false;
        var speechOutput = "I was unable to connect to Fobo. "; 
        var repromptText = "Do you want to try again? ";
        callback(session.attributes,
            buildSpeechletResponseWithoutCard(speechOutput, repromptText, shouldEndSession));
    });
}

function handleTireTemperatureRequest(intent, session, callback) {
    // Ensure that session.attributes has been initialized
    if (!session.attributes) {
        session.attributes = {};
    }

    // Set a flag to track that we're in the Help state.
    session.attributes.userPromptedToContinue = true;

    var url = "http://www.fobocloud.com/Fobo7/index.php/api/report/alexa";
    http.get(url, function(res) { 
        res.setEncoding('utf8'); 
        res.on('data', function(chunk) {
            var resultObj;
            var shouldEndSession = false;
            var speechOutput = ""; 
            var repromptText = "Please ask me for the state of one of your tires. "; 
            var position = "";
            var verticalPositionSlot = intent.slots.VerticalPosition.value;
            switch (verticalPositionSlot) {
                case "front":
                case "lead":
                case "leading":
                    position += "F";
                    break;
                case "back":
                case "aft":
                case "rear":
                    position += "R";
                    break;
            }
            var horizontalPositionSlot = intent.slots.HorizontalPosition.value;
            switch (horizontalPositionSlot) {
                case "left":
                case "left hand":
                case "left side":
                case "left hand side":
                    position += "L";
                    break;
                case "right":
                case "right hand":
                case "right side":
                case "right hand side":
                    position += "R";
                    break;
            }
            if (chunk) {
                resultObj = JSON.parse("" + chunk);
            } else {
                console.log("Unable to resolve web request to Fobo.");
            }
            if (resultObj) {
                shouldEndSession = true;
                var temperature = resultObj[position]["temp"];
                speechOutput = "Your " + verticalPositionSlot + " " + horizontalPositionSlot
                    + " tire has a temperature of " + temperature + ". ";
            }
            if (!speechOutput) { 
                speechOutput = "I was unable to process your request. "; 
            } 
            callback(session.attributes,
                buildSpeechletResponseWithoutCard(speechOutput, repromptText, shouldEndSession));
        });
    }).on('error', function (e) {
        var shouldEndSession = false;
        var speechOutput = "I was unable to connect to Fobo. "; 
        var repromptText = "Do you want to try again? ";
        callback(session.attributes,
            buildSpeechletResponseWithoutCard(speechOutput, repromptText, shouldEndSession));
    }); 
}

function handleUnsupportedRequest(intent, session, callback) {
    // Ensure that session.attributes has been initialized
    if (!session.attributes) {
        session.attributes = {};
    }

    // Set a flag to track that we're in the Help state.
    session.attributes.userPromptedToContinue = true;

    var speechOutput = "I'm sorry. I'm not equipped to help you with that. ",
        repromptText = "Is there anything else you need help with? ",
        shouldEndSession = false;

    speechOutput += repromptText;
    callback(session.attributes,
        buildSpeechletResponseWithoutCard(speechOutput, repromptText, shouldEndSession));
}

function handleRepeatRequest(intent, session, callback) {
    if (!session.attributes || !session.attributes.speechOutput) {
        getWelcomeResponse(callback);
    } else {
        callback(session.attributes,
            buildSpeechletResponseWithoutCard(session.attributes.speechOutput, session.attributes.repromptText, false));
    }
}

function handleGetHelpRequest(intent, session, callback) {
    // Ensure that session.attributes has been initialized
    if (!session.attributes) {
        session.attributes = {};
    }

    // Set a flag to track that we're in the Help state.
    session.attributes.userPromptedToContinue = true;

    var speechOutput = "I can answer questions regarding the pressure or temperature of any of your car's tires. ",
        repromptText = "Is there anything you need to know? ",
        shouldEndSession = false;

    speechOutput += repromptText;
    callback(session.attributes,
        buildSpeechletResponseWithoutCard(speechOutput, repromptText, shouldEndSession));
}

function handleFinishSessionRequest(intent, session, callback) {
    callback(session.attributes,
        buildSpeechletResponseWithoutCard("Good bye!", "", true));
}

// ------- Helper functions to build responses -------

function buildSpeechletResponse(title, output, repromptText, shouldEndSession) {
    return {
        outputSpeech: {
            type: "PlainText",
            text: output
        },
        card: {
            type: "Simple",
            title: title,
            content: output
        },
        reprompt: {
            outputSpeech: {
                type: "PlainText",
                text: repromptText
            }
        },
        shouldEndSession: shouldEndSession
    };
}

function buildSpeechletResponseWithoutCard(output, repromptText, shouldEndSession) {
    return {
        outputSpeech: {
            type: "PlainText",
            text: output
        },
        reprompt: {
            outputSpeech: {
                type: "PlainText",
                text: repromptText
            }
        },
        shouldEndSession: shouldEndSession
    };
}

function buildResponse(sessionAttributes, speechletResponse) {
    return {
        version: "1.0",
        sessionAttributes: sessionAttributes,
        response: speechletResponse
    };
}
